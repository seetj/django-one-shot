from django.shortcuts import render,get_object_or_404,redirect
from todos.models import TodoList,TodoItem
from todos.forms import TodoListForm,TodoItemForm
# Create your views here.

def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list" : list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request,id):
    task = get_object_or_404(TodoList,id=id)
    context = {
        "task_list" : task
        }
    return render(request, "todos/detail.html", context)



def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            name = form.save()
            return redirect("todo_list_detail",id= name.id)

    else:
        form = TodoListForm()
    
    context = {
        "form": form,
    }
    return render(request,"todos/create.html", context)

def todo_list_update(request,id):
    task = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST,instance=task)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail",id=id)
    else:
        form = TodoListForm(instance=task)

    context = {
        "object" : task,
        "form" : form,
    }

    return render(request, "todos/update.html",context)

def todo_list_delete(request,id):
    task = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        task.delete()
        return redirect("todo_list_list")
    
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            name = form.save()
            return redirect("todo_list_detail",id = name.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request,"todos/create_items.html",context)

def todo_item_update(request,id):
    task = get_object_or_404(TodoItem,id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST,instance=task)
        if form.is_valid():
            form.save
            return redirect("todo_list_detail",id=id)
    else:
        form = TodoItemForm(instance=task)
        
    context = {
        "object" : task,
        "form" : form,
    }

    return render(request, "todos/update_items.html",context)